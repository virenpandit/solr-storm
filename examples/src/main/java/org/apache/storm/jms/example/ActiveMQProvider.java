package org.apache.storm.jms.example;

import javax.jms.ConnectionFactory;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

import org.apache.storm.jms.JmsProvider;

@SuppressWarnings("serial")
public class ActiveMQProvider implements JmsProvider {

	private ConnectionFactory connectionFactory;
	private Destination destination;
	
	/**
	 * Constructs a <code>ActiveMQProvider</code> object given the name of the bean
	 * names of a JMS connection factory and destination.
	 * 
	 * @param connectionFactoryBean - the JMS connection factory bean name
	 * @param destinationBean - the JMS destination bean name
	 */
	public ActiveMQProvider(String connectionFactoryBean, String destinationBean){
//		this.connectionFactory = (ConnectionFactory)context.getBean(connectionFactoryBean);
//		this.destination = (Destination)context.getBean(destinationBean);
		try {
			this.connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
			Connection connection = connectionFactory.createConnection();
	        connection.start();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
	        if(destinationBean.contains("queue"))
	        	this.destination = session.createQueue(destinationBean);
	        else
	        	this.destination = session.createTopic(destinationBean);
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	public ConnectionFactory connectionFactory() throws Exception {
		return this.connectionFactory;
	}

	public Destination destination() throws Exception {
		return this.destination;
	}
}
