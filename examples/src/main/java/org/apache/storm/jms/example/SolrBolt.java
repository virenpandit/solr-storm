
package org.apache.storm.jms.example;

import java.util.Map;

import backtype.storm.topology.base.BaseRichBolt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;

import org.apache.solr.client.solrj.impl.CloudSolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.client.solrj.SolrServerException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.security.SecureRandom;
import java.math.BigInteger;
import java.util.UUID;

import com.esotericsoftware.minlog.Log;


/**
 * A generic <code>backtype.storm.topology.IRichBolt</code> implementation
 * for testing/debugging the Storm JMS Spout and example topologies.
 * <p/>
 * For debugging purposes, set the log level of the 
 * <code>backtype.storm.contrib.jms</code> package to DEBUG for debugging
 * output.
 * @author tgoetz
 *
 */
@SuppressWarnings("serial")
public class SolrBolt extends BaseRichBolt {
	private static final Logger LOG = LoggerFactory.getLogger(SolrBolt.class);
	private OutputCollector collector;
	private boolean autoAck = false;
	private boolean autoAnchor = false;
	private Fields declaredFields;
	private String name;
	
	/**
	 * Constructs a new <code>SolrBolt</code> instance.
	 * 
	 * @param name The name of the bolt (used in DEBUG logging)
	 * @param autoAck Whether or not this bolt should automatically acknowledge received tuples.
	 * @param autoAnchor Whether or not this bolt should automatically anchor to received tuples.
	 * @param declaredFields The fields this bolt declares as output.
	 */
	public SolrBolt(String name, boolean autoAck, boolean autoAnchor, Fields declaredFields){
		this.name = name;
		this.autoAck = autoAck;
		this.autoAnchor = autoAnchor;
		this.declaredFields = declaredFields;
	}
	
	public SolrBolt(String name, boolean autoAck, boolean autoAnchor){
		this(name, autoAck, autoAnchor, null);
	}

	@SuppressWarnings("rawtypes")
	public void prepare(Map stormConf, TopologyContext context,
			OutputCollector collector) {
		this.collector = collector;
		
	}

	public void execute(Tuple input) {
		LOG.debug("[" + this.name + "] Received message: " + input);

		// only emit if we have declared fields.
		if(this.declaredFields != null){
			LOG.debug("[" + this.name + "] emitting: " + input);
			if(this.autoAnchor){
				this.collector.emit(input, input.getValues());
			} else{
				this.collector.emit(input.getValues());
			}
		}
		
		LOG.debug("POSTING DATA TO SOLR >>>");
		postToSolrEngine(input);
		
		if(this.autoAck){
			LOG.debug("[" + this.name + "] ACKing tuple: " + input);
			this.collector.ack(input);
		}
		
	}

    public void postToSolrEngine(Tuple input) {
    	LOG.debug("Inside: postToSolrEngine().v.1.2");
        CloudSolrServer zkServer = null;
        try {
            zkServer = new CloudSolrServer("localhost:4181");
            zkServer.connect();
            zkServer.setDefaultCollection("tnr");
            
            LOG.debug("Creating new document...");
            SolrInputDocument doc1 = new SolrInputDocument();
            String randomId = getUUID();
            doc1.addField("id", randomId);
            doc1.addField("name", "namefield-" + getRandomString(500));
            doc1.addField("description", "description-" + getRandomString(500));
            doc1.addField("classification", "classification-" + input);

            Collection<SolrInputDocument> docs = new ArrayList<SolrInputDocument>();
            docs.add(doc1);
            
            LOG.debug("Uploading new document to Solr Engine...");
            UpdateResponse response = zkServer.add(docs);
            zkServer.commit();
            LOG.debug("Requesting index optimization...");
            zkServer.optimize();
        } catch(Exception ex) {
        	LOG.error("Error posting data to Solr Engine", ex);
        } finally {
        	try {
        		zkServer.shutdown();
        	} catch(Exception ex2) { }
        }
        LOG.debug("Exiting postToSolrEngine()");
    }
    
    public String getUUID() {
        return UUID.randomUUID().toString();
    }

    public String getRandomString(int len) {
        SecureRandom rs = new SecureRandom();
        int maxIterations=1000;
        String s="";
        int i=0;
        while(s.length()<len) {
            s+=(String) new BigInteger(230, rs).toString(len);
            i++;
        }
        return s.substring(0, len);
    }
    
	public void cleanup() {

	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		if(this.declaredFields != null){
			declarer.declare(this.declaredFields);
		}
	}
	
	public boolean isAutoAck(){
		return this.autoAck;
	}
	
	public void setAutoAck(boolean autoAck){
		this.autoAck = autoAck;
	}
}
