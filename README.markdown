## Viren: Comments
Requires: 
1. Solr Server running 4.9.0
2. Zookeeper for Solr running on port 4181
3. ActiveMQ 5.1.0 server installation

Steps -
Start ActiveMQ Server
Start Solr Server
then -
mvn clean compile install -DskipTests exec:java -Dstorm.topology=org.apache.storm.jms.example.ExampleJmsTopology
or
mvn clean -DskipTests compile install && storm jar target/storm-jms-examples-0.1-SNAPSHOT-jar-with-dependencies.jar org.apache.storm.jms.example.ExampleJmsTopology

Architecture [Topology]: https://github.com/ptgoetz/storm-jms/wiki/Example%20Topology

### End: Viren

## About Storm JMS Examples
This project contains a simple storm topology that illustrates the usage of "storm-jms".

To build:

`mvn clean install`

The default build will create a jar file that can be deployed to to a Storm cluster in the "target" directory:

`storm-jms-examples-0.1-SNAPSHOT-jar-with-dependencies.jar`

## License

Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.

## Contributors
* Viren Pandit ([@virenpandit](virenpandit@gmail.com))
